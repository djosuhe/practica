package com.crud.demoweb.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.crud.demoweb.model.Empleado;
import com.crud.demoweb.repo.IEmpleadoRepo;

@Controller
public class CrudController {

	@Autowired
	private IEmpleadoRepo repo;
	
	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("empleados", repo.findAll());
		return "index";
	}
	
	@GetMapping("/Create")
	public String create(Model model) {
		model.addAttribute(new Empleado());
		return "create";
	}
	
	@PostMapping("/Save")
	public String save(Empleado empleado, Model model) {
		repo.save(empleado);
		return "redirect:/";
	}
	
	@GetMapping("/Update/{id}")
	public String update(@PathVariable int id, Model model) {
		model.addAttribute("empleado", repo.findById(id));
		return "update";
	}
	
	@PostMapping("/Edit")
	public String edit(Empleado empleado, Model model) {
		repo.save(empleado);
		return "redirect:/";
	}
	
	@GetMapping("/Delete/{id}")
	public String delete(@PathVariable int id, Model model) {
		repo.deleteById(id);
		return "redirect:/";
	}
	
	@GetMapping("/Xml")
	public String json(Model model) {
		
		File xmlFile = new File("empleados.xml");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			try {
				Document doc = builder.parse(xmlFile);
				NodeList empleadosNodes = doc.getElementsByTagName("empleado");
				
				for(int i=0; i<empleadosNodes.getLength(); i++) {
					Node empleadoNode = empleadosNodes.item(i);
					
					if(empleadoNode.getNodeType() == Node.ELEMENT_NODE) {
						
						Element empleadoElement = (Element) empleadoNode;
						
						String nombre = empleadoElement.getElementsByTagName("nombre").item(0).getTextContent();
						String apellido = empleadoElement.getElementsByTagName("apellido").item(0).getTextContent();
						String dpi = empleadoElement.getElementsByTagName("dpi").item(0).getTextContent();
						
						Empleado empleado = new Empleado();
						empleado.setNombre(nombre);
						empleado.setApellido(apellido);
						empleado.setDpi(dpi);
						repo.save(empleado);
					}
				}
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return "redirect:/";
	}
	
	@GetMapping("/Json")
	public String json() {
		JSONParser jsonParser = new JSONParser();
		
		FileReader reader;
		try {
			reader = new FileReader("empleados.json");
			Object obj;
			try {
				obj = jsonParser.parse(reader);
				JSONArray empleados = (JSONArray) obj;
				
				for(int i=0; i<empleados.size(); i++) {
					JSONObject jsonObject = (JSONObject) empleados.get(i);
					String nombre = (String) jsonObject.get("Nombre");
					String apellido = (String) jsonObject.get("Apellido");
					String dpi = (String)jsonObject.get("Dpi");
					
					Empleado empleado = new Empleado();
					empleado.setNombre(nombre);
					empleado.setApellido(apellido);
					empleado.setDpi(dpi);
					repo.save(empleado);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:/";
	}
}