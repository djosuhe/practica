package com.crud.demoweb.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.demoweb.model.Empleado;

public interface IEmpleadoRepo extends JpaRepository<Empleado, Integer>{

}
