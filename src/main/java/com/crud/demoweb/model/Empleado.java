package com.crud.demoweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Empleado {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "empleado_Sequence")
    @SequenceGenerator(name = "empleado_Sequence", sequenceName = "EMPLEADO_SEQ")
	private int Id;
	
	@Column(name = "Nombre", length=30)
	private String Nombre;
	
	@Column(name = "Apellido", length=30)
	private String Apellido;
	
	@Column(name = "Dpi", length=15)
	private String Dpi;
	
	public int getId() {return Id;}
	
	public void setId(int id) {this.Id = id;}
	
	public String getNombre() {return Nombre;}
	
	public void setNombre(String nombre) {this.Nombre = nombre;}

	public String getApellido() {return Apellido;}
	
	public void setApellido(String apellido) {this.Apellido = apellido;}

	public String getDpi() {return Dpi;}
	
	public void setDpi(String dpi) {this.Dpi = dpi;}

}
